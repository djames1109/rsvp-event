package com.hachichu.rsvpevent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RsvpEventApplication {

    public static void main(String[] args) {
        SpringApplication.run(RsvpEventApplication.class, args);
    }

}
